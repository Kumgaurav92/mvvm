package com.techumbrella.mvvmpatternapp.util

import java.io.IOException

/**
 * Created by Gaurav Kumawat on 11-04-2020.
 */

class ApiException(message: String) : IOException(message)
class NoInternetException(message: String) : IOException(message)