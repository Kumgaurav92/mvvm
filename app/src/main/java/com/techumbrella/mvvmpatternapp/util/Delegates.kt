package com.techumbrella.mvvmpatternapp.util

import kotlinx.coroutines.*

/**
 * Created by Gaurav Kumawat on 12-04-2020.
 */

fun <T> lazyDeferred(block: suspend CoroutineScope.() -> T): Lazy<Deferred<T>> {
    return lazy {
        GlobalScope.async(start = CoroutineStart.LAZY) {
            block.invoke(this)
        }
    }
}