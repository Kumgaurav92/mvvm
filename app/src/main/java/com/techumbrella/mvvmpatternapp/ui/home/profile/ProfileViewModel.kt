package com.techumbrella.mvvmpatternapp.ui.home.profile

import androidx.lifecycle.ViewModel
import com.techumbrella.mvvmpatternapp.data.repositories.UserRepository

class ProfileViewModel(
    repository: UserRepository
) : ViewModel() {

    val user = repository.getUser()
}
