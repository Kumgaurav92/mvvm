package com.techumbrella.mvvmpatternapp.ui.home.quotes

import androidx.lifecycle.ViewModel
import com.techumbrella.mvvmpatternapp.data.repositories.QuotesRepository
import com.techumbrella.mvvmpatternapp.util.lazyDeferred

class QuotesViewModel(
    repository: QuotesRepository
) : ViewModel() {

    val quotes by lazyDeferred {
        repository.getQuotes()
    }
}
