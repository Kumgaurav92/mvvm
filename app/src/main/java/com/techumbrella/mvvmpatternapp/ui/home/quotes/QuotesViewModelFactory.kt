package com.techumbrella.mvvmpatternapp.ui.home.quotes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.techumbrella.mvvmpatternapp.data.repositories.QuotesRepository
import com.techumbrella.mvvmpatternapp.data.repositories.UserRepository

/**
 * Created by Gaurav Kumawat on 11-04-2020.
 */
@Suppress("UNCHECKED_CAST")
class QuotesViewModelFactory(
    private val repository: QuotesRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return QuotesViewModel(repository) as T
    }
}