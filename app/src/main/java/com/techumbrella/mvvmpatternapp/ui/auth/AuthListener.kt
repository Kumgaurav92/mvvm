package com.techumbrella.mvvmpatternapp.ui.auth

import androidx.lifecycle.LiveData
import com.techumbrella.mvvmpatternapp.data.db.entities.User

/**
 * Created by Gaurav Kumawat on 10-04-2020.
 */
interface AuthListener {

    fun onStarted()
    fun onSuccess(user: User)
    fun onFailure(message: String)
}