package com.techumbrella.mvvmpatternapp.ui.home.quotes

import com.techumbrella.mvvmpatternapp.R
import com.techumbrella.mvvmpatternapp.data.db.entities.Quote
import com.techumbrella.mvvmpatternapp.databinding.ItemQuoteBinding
import com.xwray.groupie.databinding.BindableItem

/**
 * Created by Gaurav Kumawat on 13-04-2020.
 */
class QuoteItem(
    private val quote: Quote
) : BindableItem<ItemQuoteBinding>() {

    override fun getLayout() = R.layout.item_quote

    override fun bind(viewBinding: ItemQuoteBinding, position: Int) {
        viewBinding.setQuote(quote)
    }
}