package com.techumbrella.mvvmpatternapp.data.network.responses

import com.techumbrella.mvvmpatternapp.data.db.entities.Quote

/**
 * Created by Gaurav Kumawat on 12-04-2020.
 */
data class QuotesResponse(

    val isSuccessful: Boolean,
    val quotes: List<Quote>
)