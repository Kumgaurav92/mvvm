package com.techumbrella.mvvmpatternapp.data.network.responses

import com.techumbrella.mvvmpatternapp.data.db.entities.User

/**
 * Created by Gaurav Kumawat on 11-04-2020.
 */
data class AuthResponse(

    var isSuccessful: Boolean?,
    var message: String?,
    var user: User?
)