package com.techumbrella.mvvmpatternapp.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Gaurav Kumawat on 12-04-2020.
 */

@Entity
data class Quote(

    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val quote: String,
    val author: String,
    val thumbnail: String,
    val created_at: String,
    val updated_at: String
)