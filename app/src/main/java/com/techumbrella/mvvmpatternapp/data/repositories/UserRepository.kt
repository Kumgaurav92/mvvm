package com.techumbrella.mvvmpatternapp.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.techumbrella.mvvmpatternapp.data.db.AppDatabase
import com.techumbrella.mvvmpatternapp.data.db.entities.User
import com.techumbrella.mvvmpatternapp.data.network.MyApi
import com.techumbrella.mvvmpatternapp.data.network.SafeApiRequest
import com.techumbrella.mvvmpatternapp.data.network.responses.AuthResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Gaurav Kumawat on 10-04-2020.
 */
class UserRepository(
    private val api: MyApi,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun userLogin(email: String, password: String): AuthResponse {
        return apiRequest { (api.userLogin(email, password)) }
    }

    suspend fun userSignUp(name: String, email: String, password: String): AuthResponse {
        return apiRequest { (api.userSignUp(name, email, password)) }
    }

    suspend fun saveUser(user: User) = db.getUserDao().upsert(user)

    fun getUser() = db.getUserDao().getuser()
}