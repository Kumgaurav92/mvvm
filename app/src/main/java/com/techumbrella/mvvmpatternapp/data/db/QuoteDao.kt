package com.techumbrella.mvvmpatternapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.techumbrella.mvvmpatternapp.data.db.entities.CURRENT_USER_ID
import com.techumbrella.mvvmpatternapp.data.db.entities.Quote
import com.techumbrella.mvvmpatternapp.data.db.entities.User

/**
 * Created by Gaurav Kumawat on 11-04-2020.
 */
@Dao
interface QuoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllQuotes(quotes: List<Quote>)

    @Query("SELECT * FROM Quote")
    fun getQuotes(): LiveData<List<Quote>>
}