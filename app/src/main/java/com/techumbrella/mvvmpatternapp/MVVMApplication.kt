package com.techumbrella.mvvmpatternapp

import android.app.Application
import com.techumbrella.mvvmpatternapp.data.db.AppDatabase
import com.techumbrella.mvvmpatternapp.data.network.MyApi
import com.techumbrella.mvvmpatternapp.data.network.NetworkConnectionInterceptor
import com.techumbrella.mvvmpatternapp.data.preferences.PreferenceProvider
import com.techumbrella.mvvmpatternapp.data.repositories.QuotesRepository
import com.techumbrella.mvvmpatternapp.data.repositories.UserRepository
import com.techumbrella.mvvmpatternapp.ui.auth.AuthViewModel
import com.techumbrella.mvvmpatternapp.ui.auth.AuthViewModelFactory
import com.techumbrella.mvvmpatternapp.ui.home.profile.ProfileViewModelFactory
import com.techumbrella.mvvmpatternapp.ui.home.quotes.QuotesViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

/**
 * Created by Gaurav Kumawat on 12-04-2020.
 */
class MVVMApplication : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@MVVMApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { UserRepository(instance(), instance()) }
        bind() from singleton { QuotesRepository(instance(), instance(), instance()) }
        bind() from provider { AuthViewModelFactory(instance()) }
        bind() from provider { ProfileViewModelFactory(instance()) }
        bind() from provider { QuotesViewModelFactory(instance()) }
    }
}